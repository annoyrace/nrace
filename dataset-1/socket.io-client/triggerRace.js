/* Control behavior using DISCONNECT_DELAY{1,2}.
 * Samples: 
 *   DISCONNECT_DELAY1=0 DISCONNECT_DELAY2=200 ./node_modules/.bin/mocha --reporter=spec test/disconnect.js
 *   DISCONNECT_DELAY1=100 DISCONNECT_DELAY2=0 ./node_modules/.bin/mocha --reporter=spec test/disconnect.js
 *   DISCONNECT_DELAY1=100 DISCONNECT_DELAY2=200 ./node_modules/.bin/mocha --reporter=spec test/disconnect.js
 *   DISCONNECT_DELAY1=0 DISCONNECT_DELAY2=0 ./node_modules/.bin/mocha --reporter=spec test/disconnect.js
 */

var http = require('http').Server;
var io = require('socket.io');
//var fs = require('fs');
//var join = require('path').join;
var ioc = require('./');
//var request = require('supertest');
//var expect = require('expect.js');

console.log("Test starting up");

// var delay1 = process.env.DISCONNECT_DELAY1 || 0;
var delay1 = 0;
// var delay2 = process.env.DISCONNECT_DELAY2 || 0;
// changed to 51 to avoid revealing the error
var delay2 = 51;
thetest(delay1, delay2);

function thetest (delay1, delay2)
{
  console.log("thetest: delay1 " + delay1 + " delay2 " + delay2);

  console.log("thetest: defining srv, sio, and total");
  var srv = http();
  var sio = io(srv);
  var total = 8;

  function cleanup ()
  {
    console.log("thetest: done, cleaning up and exiting");
    srv.close(); /* Prevent EADDRINUSE complaints on subsequent runs. */
    process.exit(0);
  }

  srv.listen(8080, "0.0.0.0", function(){
    /* Define server behavior. */
    sio.of('/chat').on('connect', function(s) {
      console.log('server connected socket on /chat', s.client.id);
      total = countdown(total, srv);
      s.on('disconnect', function() {
        console.log('server disconnected socket on /chat', s.client.id);
        total = countdown(total, cleanup);
      });
    });
    sio.of('/news').on('connect', function(s) {
      console.log('server connected socket on /news', s.client.id);
      total = countdown(total, cleanup);
      s.on('disconnect', function() {
        console.log('server disconnected socket on /news', s.client.id);
        total = countdown(total, cleanup);
      });
    });

    /* Create a chat client. After getting a 'connect', disconnect after delay2 ms. */
    console.log('client1 connecting to /chat')
    var chat = client(srv, '/chat');
    chat.on('connect', function(){
      total = countdown(total, cleanup);
      console.log('client1 connected socket on /chat.');

      setTimeout(function() {
        console.log('client1 disconnecting socket on /chat.');
        total = countdown(total, cleanup);
        chat.disconnect();
      }, delay2);
    });

    /* After delay1 ms, create a news client. After getting a 'connect', disconnect after delay2. */
    setTimeout(function() {
      console.log('client2 connecting to /news')
      var news = client(srv, '/news');
      news.on('connect', function(){
        total = countdown(total, cleanup);
        console.log('client2 connected socket on /news.')

        setTimeout(function() {
          console.log('client2 disconnecting socket on /news.');
          total = countdown(total, cleanup);
          news.disconnect();
        }, delay2);

      });
    }, delay1);
  });
}

// creates a socket.io client for the given server
function client (srv, nsp, opts)
{
  if ('object' == typeof nsp)
  {
    opts = nsp;
    nsp = null;
  }

  var addr = srv.address();
  if (!addr)
    addr = srv.listen().address();
  var url = 'ws://' + addr.address + ':' + addr.port + (nsp || '');
  return ioc(url, opts);
  console.log("print me");
}

/* Input: nLeft
 * Output: --nLeft
 * When --nLeft == 0, we call doneCB() for any cleanup you desire. doneCB() should not return.
 */
function countdown (nLeft, doneCB)
{
  --nLeft;
  if (nLeft <= 0)
  {
    console.log("Test finished");
    doneCB();
    return -1;
  }
  return nLeft;
}

