try{
    require('./_known_bug.js');
    console.log('testing known bug by injecting non-determinism to its patching unit test.');
    return;
}catch(e){

}

try{
    require('./_known_bug_sample.js');
    console.log('testing known bug by creating testing script according to the bug report and inject non-determinism');
}catch(e){

}

console.log('ERROR: no testing script named _known_bug.js or _known_bug_sample.js is found.');
