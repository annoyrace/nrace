var assert = require("assert-plus"),
	util = require("util"),
	endpoint = require("../index"),
	fs = require("fs"),
	lib = require("./lib");

describe("triggerRace", function(){
	describe("()", function() {
		it("should create 6 files and roll 5", function(done) {
                        this.timeout(0); // disable timeout
			var log = {
				level: "debug",
				date: new Date(),
				pid: 123,
				origin: "script",
				message: "test",
				metadata: {a: 1},
				fullOrigin: {
					file: "test.js",
					line: 123,
					fn: "testfn"
				}
			}; // 173B each
			var rolls = 0, creates = 0;
			endpoint(true, true, true, true, "./test/log", "triggerrace_", ".txt", 173*2-1/*hold 10 logs per file*/, 60 * 60, 10, function(err, e) {
				if (err) {
					throw err;
				} else {
					e.on("rollFile", function(file) {
						rolls += 1;
					});
					e.on("createFile", function(file) {
						creates += 1;
					});
					lib.logMultipleTimesHeavy(e, log, 3, function(err) { // log 50 times --> 5 rolls & 6 creates
						if (err) {
							throw err;
						} else {
							e.stop(function() {
								if (err) {
									throw err;
								} else {
									assert.equal(creates, 2, "6 creates expected");
									assert.equal(rolls, 1, "5 rolls expected");
									done();
								}
							});
						}
					});
				}
			});
		});
	});
});
