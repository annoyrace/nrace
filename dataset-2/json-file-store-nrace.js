var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _chai = require('chai');

var _chai2 = _interopRequireDefault(_chai);

var _Store = require('./Store');

var _Store2 = _interopRequireDefault(_Store);

var _child_process = require('child_process');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var NAME = ".specTests";

var store = new _Store2.default(NAME, {
    type: 'single',
    //pretty: true
});

var d1 = {
    x: 0.6
};
var d2 = {
    z: -3
};

store.save('id', d1, function cb1 () {
    //console.log('save d1: %d', id);
});

store.save(d2, function cb2 () {
    //console.log('save d2: %d', _id);
    //id = _id;
});

store.get('id', function (err, o) {
    //console.log('get: %s', o);
});

var data = {
    y: 88
};

store.save('del', data, function (id) {
    
});

store['delete']('del', function () {
    //console.log('delete');
});