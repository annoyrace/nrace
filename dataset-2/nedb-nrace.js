var should = require('chai').should()
  , assert = require('chai').assert
  , testDb = './test/workspace/test.db'
  , fs = require('fs')
  , path = require('path')
  , _ = require('underscore')
  , async = require('async')
  , model = require('./lib/model')
  , Datastore = require('./lib/datastore')
  , Persistence = require('./lib/persistence')
  , reloadTimeUpperBound = 60;   // In ms, an upper bound for the reload time used to check createdAt and updatedAt
  ;

var d = new Datastore({ filename: testDb, autoload: true });

var docs = [{ a: 5, b: 'hello' }, { a: 42, b: 'world' }];

/*var fileStr = model.serialize({ _id: '1', a: 5, planet: 'Earth' }) + '\n' + model.serialize({ _id: '2', a: 5, planet: 'Mars' }) + '\n'
        , autoDb = './test/workspace/auto.db'
        , db
        ;

fs.writeFileSync(autoDb, fileStr, 'utf8');*/
var autoDb = './test/workspace/auto.db';
var db = new Datastore({ filename: autoDb, autoload: true })

db.find({}, function (err, docs) {
    console.log(docs);
});

db.remove({ _id: '2' }, {}, function (err, numRemoved) {
    // numRemoved = 1
    console.log('remove: %d', numRemoved);
});

db.find({}, function (err, docs) {
    console.log(docs);
});