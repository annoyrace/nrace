var fs = require('fs')
var os = require('os')
var path = require('path')
var rimraf = require('rimraf')
var jf = require('./')

function _read () {

    var TEST_DIR = path.join(os.tmpdir(), 'jsonfile-tests-readfile')
    rimraf.sync(TEST_DIR)
    fs.mkdir(TEST_DIR, function done () {
        var file = path.join(TEST_DIR, 'somefile.json')
        var obj = { name: 'JP' }
        fs.writeFileSync(file, JSON.stringify(obj))

        jf.readFile(file, (err, obj2) => {
            rimraf.sync(TEST_DIR)
        })
    })

}

function _write () {
    var TEST_DIR = path.join(os.tmpdir(), 'jsonfile-tests-writefile')
    rimraf.sync(TEST_DIR)
    fs.mkdir(TEST_DIR, function done () {
        var file = path.join(TEST_DIR, 'somefile2.json')
        var obj = { name: 'JP' }

        jf.writeFile(file, obj, (err) => {

            fs.readFile(file, 'utf8', (err, data) => {
                rimraf.sync(TEST_DIR)
            })
        })
    })
}

//_read();
_write();