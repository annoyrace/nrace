// https://github.com/sindresorhus/del/issues/43
// The bug depends on a very old version of rimraf (I just modified the package.json for the 
// right version of rimraf)

var assert = require('assert');
var fsextra = require('fs-extra');
var pathExists = require('path-exists');
var del = require('./');

var fixtures = [
    'folder/1.tmp',
    'folder/2.tmp'
];
// before
fixtures.forEach(fsextra.ensureFileSync);

try {
    del('folder/**').then(function () {
        assert(!pathExists.sync('folder/1.tmp'));
        assert(!pathExists.sync('folder/2.tmp'));
    })
    .catch(function (err) {
        console.log(err);    
    });
}
catch (err) {
    console.log(err);
}
