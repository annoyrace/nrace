var lineReader                    = require('./lib/line_reader'),
    assert                        = require('assert'),
    fs                            = require('fs'),
    testFilePath                  = __dirname + '/test/data/normal_file.txt',
    windowsFilePath               = __dirname + '/data/windows_file.txt',
    windowsBufferOverlapFilePath  = __dirname + '/data/windows_buffer_overlap_file.txt',
    unixFilePath                  = __dirname + '/data/unix_file.txt',
    macOs9FilePath                = __dirname + '/data/mac_os_9_file.txt',
    separatorFilePath             = __dirname + '/data/separator_file.txt',
    multiSeparatorFilePath        = __dirname + '/data/multi_separator_file.txt',
    multibyteFilePath             = __dirname + '/data/multibyte_file.txt',
    emptyFilePath                 = __dirname + '/data/empty_file.txt',
    oneLineFilePath               = __dirname + '/data/one_line_file.txt',
    oneLineFileNoEndlinePath      = __dirname + '/data/one_line_file_no_endline.txt',
    threeLineFilePath             = __dirname + '/data/three_line_file.txt',
    testSeparatorFile             = ['foo', 'bar\n', 'baz\n'],
    testFile = [
      'Jabberwocky',
      '',
      '’Twas brillig, and the slithy toves',
      'Did gyre and gimble in the wabe;',
      '',
      ''
    ],
    testBufferOverlapFile = [
      'test',
      'file'
    ];

lineReader.open(testFilePath, function testCb (err, reader) {
    /*for (var i = 0; i < 5; i++) {
        reader.nextLine(function (err, line) {
            console.log("%d: %s", i, line);
        })
    }*/
    reader.nextLine(function nextlinecb1 (err, line) {
        //console.log('1: %s',line);
    });
    reader.nextLine(function nextlinecb2 (err, line) {
        //console.log('2: %s',line);
    });
    reader.nextLine(function nextlinecb3 (err, line) {
        //console.log('3: %s',line);
    });
    reader.nextLine(function nextlinecb4 (err, line) {
        //console.log('4: %s',line);
    });
    reader.nextLine(function nextlinecb5 (err, line) {
        //console.log('5: %s',line);
    });
    reader.close(function closecb () {});
})