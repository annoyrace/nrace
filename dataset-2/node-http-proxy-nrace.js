var httpProxy = require('./lib/http-proxy'),
    expect    = require('expect.js'),
    http      = require('http'),
    net       = require('net'),
    ws        = require('ws'),
    io        = require('socket.io'),
    SSE       = require('sse'),
    ioClient  = require('socket.io-client');

//
// Expose a port number generator.
// thanks to @3rd-Eden
//
var initialPort = 1024, gen = {};
Object.defineProperty(gen, 'port', {
  get: function get() {
    return initialPort++;
  }
});

var ports = { source: gen.port, proxy: gen.port };
var proxy = httpProxy.createProxyServer({
  target: 'ws://127.0.0.1:' + ports.source,
  ws: true
}),
proxyServer = proxy.listen(ports.proxy);
proxy.on('proxyReqWs', function(proxyReq, req, socket, options, head) {
    console.log('0.event proxy proxyReqWs');
    proxyReq.setHeader('X-Special-Proxy-Header', 'foobar');
});

destiny = new ws.Server({ port: ports.source }, function () {
  
});

//should pass all set-cookie headers to client
destiny.on('headers', function (headers) {
    console.log('event headers');
    headers.push('Set-Cookie: test1=test1');
    headers.push('Set-Cookie: test2=test2');
});

destiny.on('connection', function (socket, upgradeReq) {
    console.log('3.event srv connection');
    expect(upgradeReq.headers['x-special-proxy-header']).to.eql('foobar');

    socket.on('message', function (msg) {
        console.log('4.event srv msg');
        expect(msg).to.be('hello there');
        socket.send('Hello over websockets');
    });
});

var key = new Buffer(Math.random().toString()).toString('base64');

var requestOptions = {
    port: ports.proxy,
    host: '127.0.0.1',
    headers: {
    'Connection': 'Upgrade',
    'Upgrade': 'websocket',
    'Host': 'ws://127.0.0.1',
    'Sec-WebSocket-Version': 13,
    'Sec-WebSocket-Key': key
    }
};

var req = http.request(requestOptions);

req.on('upgrade', function (res, socket, upgradeHead) {
    console.log('event upgrade')
    expect(res.headers['set-cookie'].length).to.be(2);
    //done();
});

req.end();

var client = new ws('ws://127.0.0.1:' + ports.proxy);

client.on('open', function () {
    console.log('1.event client open');
    client.send('hello there');
});

client.on('message', function (msg) {
    console.log('2.event client msg');
    expect(msg).to.be('Hello over websockets');
    
    //done();
});

setTimeout(function () {
    client.close();
    proxyServer.close();
    destiny.close();
}, 3000);