// Load modules

var fs = require('fs');

var Code = require('code');
var Hapi = require('hapi');
var Lab = require('lab');
var Nes = require('../');

// Declare internals

var internals = {};


// Test shortcuts

var lab = exports.lab = Lab.script();
var describe = lab.describe;
var it = lab.it;
var expect = Code.expect;


describe('Browser', function () {

    describe('Client', function () {

      describe('_reconnect()', function () {

        /* This is my modified test case that non-deterministically exposes the bug. 
         * The bug will manifest under the following circumstances:
         *   - close() -> disconnect() (triggering reconnection via a reconnectionDelay timer)
         *      This is always true because we make it happen deliberately.
         *      NB The reconnectionDelay timer is launched after the disconnect() completes, which requires another event on the epoll.
         *   - disconnect() -> the reconnectionDelay timer
         *   (- The reconnectionDelay timer goes off before the server notices that we're finished and calls done(),
         *      but this seems likely unless you do some serious timer fuzzing.)
         **/
        it('Requests 1 close and 1 disconnect', function (done) {

          var checkForStopFreq = 300;

          var reconnectionDelay = 20;
  
          console.log("reconnectionDelay %d", reconnectionDelay);

          var triedDisconnect = 0;
          var triedClose = 0;

          var connectionCount = 0;

          var server = new Hapi.Server();
          server.connection();
          server.register({ register: Nes, options: { auth: false } }, function (err) {

            setTimeout(function checkForStop (){
              if (triedDisconnect && triedClose)
              {
                /* Wait a bit more to make sure any pending CBs registered by disconnect and close have gone off.
                 * Make sure we might actually see something interesting. */
                setTimeout(function(){
                  console.log(Date.now() + ": checkForStop: server.stop'ing");
                  server.stop(done);
                }, checkForStopFreq);
              }
              else
                setTimeout(checkForStop, checkForStopFreq);
            }, checkForStopFreq);

            server.start(function (err) {

              console.log(Date.now() + ": Entering server.start CB");
              var client = new Nes.Client('http://localhost:' + server.info.port);

              /* Define the onConnect behavior for the client. */
              client.onConnect = function () {
                console.log(Date.now() + ": client.onConnect");
                /* Only close/disconnect once. */
                connectionCount++;
                if (1 < connectionCount)
                  return;

                /* Close the connection. 
                 * This will go to client.js's Client._onClose function, which calls _reconnect, 
                 *   which sets a timer that calls _connect "after a bit" - namely, reconnectionDelay.
                 */
                console.log(Date.now() + ": Calling client._ws.close()");
                client._ws.close();
                triedClose = 1;

                /* Disconnect the connection "in a bit". */
                fs.stat("/tmp", function doDisconnect () {
                  /* Disconnect the connection. If there is a pending _reconnect operation, it should be discarded when its timer actually goes off. 
                   * if (reconnectionDelay < disconnectDelay), then the reconnection will finish before we disconnect. */
                   console.log(Date.now() + ": doDisconnect: client.disconnect()");
                   client.disconnect();
                   triedDisconnect = 1;
                }); 
              };

              /* Start the connection. Choose delay large enough that we disconnect before the reconnection happens. */
              //client.connect({}, function () { });
              client.connect({ delay: reconnectionDelay }, function () { });
            });

          });
        });
      });
    });

});
