var fs = require('fs');

exports.mkdirp = exports.mkdirP = function mkdirP (p, mode, f) {
    var cb = f || function () {};
    if (p.charAt(0) != '/') { cb(new Error('Relative path: ' + p)); return }
    
    var ps = p.split('/');
    fs.stat(p, function (err, stats) {
        if (err) 
        {
          mkdirP(ps.slice(0,-1).join('/'), mode, function (err) {
            if (err && err.errno != process.EEXIST) cb(err);
            else fs.mkdir(p, mode, cb);
          });
        }
        else cb(null);
    });
};
