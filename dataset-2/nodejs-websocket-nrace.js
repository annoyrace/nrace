var ws = require('./index');
var TEST_PORT = 8017;

var testConn;
var testServer = ws.createServer(function (conn) {
    testConn = conn;
}).listen(TEST_PORT);

testServer.once('connection', function (client) {
    console.log(client.headers['x-headername']);
    //client.headers['x-headername'].should.be.equal('header value')
    //done()
})

ws.connect('ws://localhost:' + TEST_PORT, {
    extraHeaders: {
        'X-HeaderName': 'header value'
    }
})

setTimeout(function () {
    testConn.close();
    testServer.close();
},1000);