var path = require('path');
var rimraf = require('rimraf');
var ncp = require('../').ncp;

var fixtures = path.join(__dirname, 'regular-fixtures'),
    src = path.join(fixtures, 'src'),
    out = path.join(fixtures, 'out');

rimraf(out, function () {
    ncp(src, out, cb);
});

function cb(params) {
    
}