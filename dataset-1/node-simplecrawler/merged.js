/**
 * https://github.com/simplecrawler/simplecrawler/issues/298
 * 
 * Merge the server and crawler code in one file
 * 
 * - Comment code that induces the bug
 * - Add some error messages for expected behavior
 */
'use strict';

var express = require('express');
var app     = express();
var path    = require('path');
var compression = require('compression');

// var SLEEP_TIME = 3000;

app.use(compression({threshold: 0}));

app.get('/',function(req,res){
    // setTimeout(function () {
        res.sendFile(path.join(__dirname + '/index.html'))
    // }, SLEEP_TIME);
});

app.get('/p1',function(req,res){
    res.sendFile(path.join(__dirname + '/p1.html'));
});

app.get('/p2',function(req,res){
    res.sendFile(path.join(__dirname + '/p2.html'));
});

app.listen(32714);

console.log('Server is running');

//starts client code

var Crawler = require('./');

//var crawler = new Crawler('127.0.0.1', '/', 32714);
var crawler = new Crawler('http://127.0.0.1:32714/')

// crawler.interval = 1;
// crawler.maxConcurrency = 1;
var order = [];

crawler.on('queueadd', function (queueItem) {
    order.push('queueadd ' + queueItem.url);
    console.log('Added %s to queue', queueItem.url);
});

crawler.on('fetchcomplete', function (queueItem, responseBuffer, response) {
    order.push('fetchcomplete ' + queueItem.url);
    console.log('Received %s', queueItem.url);
});

crawler.on('complete', function () {
    console.log('Crawl complete');
    //expect  events of 'queueadd' and 'fetchcomplete' before event 'complete'
    if (order.length !== 5) {         
        console.log('Error: no enough events performed before');
    }
    process.exit();
});

crawler.start();

console.log('Crawler has started');